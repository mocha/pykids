#!/usr/bin/python

example_dictionary = [ 1, 2, 3, 4 ]

dict_with_words = [ "apples", "oranges", "cherries", "bananas" ]

# we can also reassign dictionaries to new variables

fruits = dict_with_words

# this library does things with randomness, we'll use it to choose something at random
import random
print("Here's a random fruit: " + random.choice(fruits))

# we can also add things to a dictionary
fruits.append("pears")

# we can see how many things are in it with "len" (for length)
len(fruits)

# if we add more fruits, the length increases
fruits.append("grapes")
fruits.append("limes")
fruits.append("tangerines")
len(fruits)

# the "for" statement lets us do things with everything in a set of stuff
for fruit in fruits:
    print("I love " + fruit + "!")
